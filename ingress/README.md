官方参考链接
https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-manifests/

注意：
集群必须先有node节点，否则pod无法运行。
kubectl apply -f ns-and-sa.yaml
kubectl apply -f rbac.yaml
kubectl apply -f default-server-secret.yaml
kubectl apply -f nginx-config.yaml
kubectl apply -f nginx-ingress.yaml

测试用例测试页面
cd nginx
kubectl apply -f nginx.yaml
kubectl apply -f nginx-service.yaml
kubectl apply -f nginx-ingress.yaml