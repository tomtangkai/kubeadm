# 一 k8s搭建命令
### 1 master init
sh kubeadm.sh --node-type master --master-address 192.168.100.80
### 2 node join 在node节点执行下面的命令
sh kubeadm.sh --node-type node --master-address 192.168.100.80



# 二 dashboard 部署命令
### 1 创建dashboard  本清单使用nodeport  32443，可自行修改
kubectl create -f kubernetes-dashboard.yaml
### 2 创建token
kubectl create -f adminuser.yaml
### 3 查看运行的节点和端口
kubectl get pod -n kubernetes-dashboard -o wide
kubectl get service -n kubernetes-dashboard -o wide
###  4 获取token
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')